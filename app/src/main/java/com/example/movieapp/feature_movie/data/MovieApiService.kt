package com.example.movieapp.feature_movie.data

import com.example.movieapp.feature_movie.domain.model.Movie
import com.example.movieapp.feature_movie.domain.model.OrderMovie
import retrofit2.http.GET
import retrofit2.http.Path

interface MovieApiService {
    @GET("movie/popular")
    suspend fun getPopularMovie(): MovieResponse

    @GET("movie/{id}")
    suspend fun getMovieById(
        @Path("id") id: Long
    ): Movie
}