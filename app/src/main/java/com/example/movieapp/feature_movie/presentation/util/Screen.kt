package com.example.movieapp.feature_movie.presentation.util

sealed class Screen(val route: String) {
    object Home : Screen("home")
    object Notifikasi : Screen("notif")
    object Profie : Screen("profile")


//    object DetailMovie : Screen("detail")

    object DetailMovie : Screen("home/{movieId}") {
        fun createRoute(movieId: Long) = "home/$movieId"
    }
}