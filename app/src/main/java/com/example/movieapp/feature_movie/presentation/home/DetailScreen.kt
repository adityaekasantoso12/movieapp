package com.example.movieapp.feature_movie.presentation.home

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.rememberImagePainter
import com.example.movieapp.feature_movie.presentation.util.UiState
import com.example.noteapp.R
import org.koin.androidx.compose.getViewModel

@Composable
fun DetailScreen(
    movieId: Long,
    viewModel: DetailMovieViewModel = getViewModel(),
    navigateBack: () -> Unit,

) {
    viewModel.uiState.collectAsState(initial = UiState.Loading).value.let { uiState ->
        when (uiState) {
            is UiState.Loading -> {
                viewModel.getMovieById(movieId)
            }
            is UiState.Success -> {
                val data = uiState.data
                DetailContent(
                    data.movie.title,
                    data.movie.backdrop_pathurl,
                    data.movie.overview,
                    onBackClick = navigateBack,
                )
            }
            is UiState.Error -> {
            }
        }
    }
}

@Composable
fun DetailContent(
    title: String,
    backdrop_pathurl: String,
    overview: String,
    onBackClick: () -> Unit,

    ) {
    LazyColumn(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp)
    ) {
        item {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(200.dp)
                    .background(Color.Gray)
            ) {
                Image(
                    painter = rememberImagePainter(backdrop_pathurl),
                    contentDescription = null,
                    contentScale = ContentScale.Crop,
                    modifier = Modifier.fillMaxSize()
                )
                Icon(
                    imageVector = Icons.Default.ArrowBack,
                    contentDescription = stringResource(R.string.back),
                    modifier = Modifier.padding(16.dp).clickable { onBackClick() }
                )
                Text(
                    text = title,
                    style = MaterialTheme.typography.titleLarge.copy(
                        fontWeight = FontWeight.Bold
                    ),
                    color = Color.White,
                    modifier = Modifier
                        .align(Alignment.BottomStart)
                        .padding(14.dp)
                )
            }
        }
        item {
            Spacer(modifier = Modifier.height(16.dp))

            Text(
                text = "Overview",
                style = MaterialTheme.typography.bodyLarge.copy(
                    fontWeight = FontWeight.SemiBold
                ),
            )

            Spacer(modifier = Modifier.height(5.dp))

            Text(
                text = overview,
                style = MaterialTheme.typography.bodyMedium,
                color = MaterialTheme.colorScheme.onBackground
            )
        }
    }
}


@Preview(showBackground = true)
@Composable
fun DetailContentPreview() {
    MaterialTheme {
        DetailContent(
            backdrop_pathurl = "",
            title = "Barbie The Movie",
            overview = "Barbie adalah boneka yang diproduksi oleh perusahaan Amerika Serikat, Mattel, dan diperkenalkan pada Maret 1959. Ruth Handler, pembuat boneka ini mendapatkan inspirasi dari sebuah boneka asal Jerman yang bernama Bild Lilli.",
            onBackClick = {}
        )
    }
}