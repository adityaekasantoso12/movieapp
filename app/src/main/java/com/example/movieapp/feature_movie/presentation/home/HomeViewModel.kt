package com.example.movieapp.feature_movie.presentation.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.movieapp.feature_movie.domain.model.OrderMovie
import com.example.movieapp.feature_movie.presentation.util.UiState
import com.example.movieapp.feature_movie.data.MovieRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.launch

class HomeViewModel(
    private val repository: MovieRepository
) : ViewModel() {
    private val _uiState: MutableStateFlow<UiState<List<OrderMovie>>> = MutableStateFlow(UiState.Loading)

    val uiState: StateFlow<UiState<List<OrderMovie>>>
        get() = _uiState
    fun getPopularMovie() {
        viewModelScope.launch {
            repository.getAllMovie()
                .catch {
                    _uiState.value = UiState.Error(it.message.toString())
                }
                .collect { orderMovie ->
                    _uiState.value = UiState.Success(orderMovie)
                }
        }
    }
}