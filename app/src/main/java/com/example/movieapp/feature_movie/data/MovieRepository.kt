package com.example.movieapp.feature_movie.data

import com.example.movieapp.feature_movie.domain.model.Movie
import com.example.movieapp.feature_movie.domain.model.OrderMovie
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.flowOn

class MovieRepository {

    suspend fun getAllMovie(): Flow<List<OrderMovie>> {
        return flow {
            val movieResponse = MovieApi.retrofitService.getPopularMovie()
            val orderMovies = movieResponse.results.map {
                OrderMovie(it)
            }
            emit(orderMovies)
        }
    }

    suspend fun getMovieById(id: Long): OrderMovie {
        val movie = MovieApi.retrofitService.getMovieById(id)
        return OrderMovie(movie)
    }

    companion object {
        @Volatile
        private var instance: MovieRepository? = null

        fun getInstance(): MovieRepository =
            instance ?: synchronized(this) {
                MovieRepository().apply {
                    instance = this
                }
            }
    }
}
