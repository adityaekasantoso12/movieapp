package com.example.movieapp.feature_movie.domain.model

import kotlinx.serialization.Serializable

@Serializable
data class OrderMovie(
    val movie: Movie
)