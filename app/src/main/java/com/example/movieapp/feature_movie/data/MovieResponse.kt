package com.example.movieapp.feature_movie.data

import com.example.movieapp.feature_movie.domain.model.Movie
import kotlinx.serialization.Serializable

@Serializable
data class MovieResponse(
    val results: List<Movie>
)