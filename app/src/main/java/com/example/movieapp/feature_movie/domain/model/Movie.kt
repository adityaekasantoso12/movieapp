package com.example.movieapp.feature_movie.domain.model

import kotlinx.serialization.Serializable

@Serializable
data class Movie(
    val id: Long,
    val poster_path: String,
    val title: String,
    val overview: String,
    val release_date: String,
    val backdrop_path: String,
) {
    val imageUrl: String
        get() = "https://image.tmdb.org/t/p/w500/$poster_path"
    val backdrop_pathurl: String
        get() = "https://image.tmdb.org/t/p/w500/$poster_path"

}
