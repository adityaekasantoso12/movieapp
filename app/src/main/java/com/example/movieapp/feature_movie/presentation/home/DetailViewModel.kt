package com.example.movieapp.feature_movie.presentation.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.movieapp.feature_movie.data.MovieRepository
import com.example.movieapp.feature_movie.domain.model.OrderMovie
import com.example.movieapp.feature_movie.presentation.util.UiState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class DetailMovieViewModel(
    private val repository: MovieRepository
) : ViewModel() {
    private val _uiState: MutableStateFlow<UiState<OrderMovie>> =
        MutableStateFlow(UiState.Loading)
    val uiState: StateFlow<UiState<OrderMovie>>
        get() = _uiState

    fun getMovieById(movieId: Long) {
        viewModelScope.launch {
            _uiState.value = UiState.Loading
            _uiState.value = UiState.Success(repository.getMovieById(movieId))
        }
    }
}