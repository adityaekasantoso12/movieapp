package com.example.movieapp.di

import com.example.movieapp.feature_movie.data.MovieRepository
import com.example.movieapp.feature_movie.presentation.home.DetailMovieViewModel
import com.example.movieapp.feature_movie.presentation.home.HomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    single { MovieRepository.getInstance() }
    viewModel { HomeViewModel(get()) }
    viewModel { DetailMovieViewModel(get()) }
}
